import React from 'react'
import './header.css'
import SearchIcon from '@material-ui/icons/Search'
import { ShoppingBasket } from '@material-ui/icons'
import { Link } from 'react-router-dom'

function Header() {
    return (
        <div className="Header">
            {/* Logo */}
            <Link to="/">
                <img
                    className="header_logo"
                    src="http://pngimg.com/uploads/amazon/amazon_PNG11.png" />
            </Link>

            {/* Search */}
            <div
                className="header_search"
            >
                <input className="header_searchInput" type="text" />
                <SearchIcon className="header_searchIcon" />
            </div>

            {/* Menu */}
            <div
                className="header_nav"
            >
                <div className="header_option">
                    <span className="header_optionLineOne">Hello Drilon</span>
                    <span className="header_optionLineTwo">Sign In</span>
                </div>
                <div className="header_option">
                    <span className="header_optionLineOne">Returns</span>
                    <span className="header_optionLineTwo">& Orders</span>
                </div>
                <div className="header_option">
                    <span className="header_optionLineOne">Your</span>
                    <span className="header_optionLineTwo">Prime</span>
                </div>
                <div className="header_optionBasket">
                    <ShoppingBasket className="header_basketIcon" />
                    <span className="header_optionLineTwo header_basketCount">0</span>
                </div>

            </div>
        </div>
    )
}

export default Header
