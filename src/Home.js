import React from 'react'
import './home.css'
import Product from './Product'


function Home() {
    return (
        <div className="home">
            <div className="home_container">
                <img className="home_image" src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg" />
                <div className="home_row">
                    <Product
                        title="The lean startup - Vuoi programmare? Questo libro non fa per te" 
                        price={29.99} 
                        image={"https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._AC_SY400_.jpg"} 
                        rating={5} 
                    />
                    <Product
                        title="Padre ricco padre povero - 
                        Un libro che ha venduto più di 1 milione 
                        di copie in tutto il mondo, 
                        un MUST HAVE per poter cambiare il tuo mindset vincente" 
                        price={14.99} 
                        image={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSJwqba9vSIq5PJZQd48CbdhCNJ3-HbbR5Wb3irBfuTKp0p1USHZmbolGrueVQ&usqp=CAc"} 
                        rating={2} 
                    />
                </div>
                <div className="home_row"> 
                    <Product
                        title="SanDisk SSD - 550gb di pura potenza
                        per poter gestire meglio i tuoi lavori" 
                        price={129.99} 
                        image={"https://images.eprice.it/nobrand/0/Lightbox/424/201864424/2420EU125216PRDID.2.jpg"} 
                        rating={4} 
                    />
                    <Product
                        title="Yu-gi-oh! Le carte collezionabili che hanno 
                        conquistato il mondo. 
                        Compra il set da 25 pacchetti" 
                        price={23.99} 
                        image={"https://www.yugioh-card.com/products_publish/eu/sbad/eu/it/images/sbad_foil_mocks_it.jpg"} 
                        rating={5} 
                    />
                    <Product
                        title="Bose Cuffie QuietComfort 35 - Le migliori cuffie sul mercato a portata di CLICK" 
                        price={74.99} 
                        image={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSMXjKZNQq6HiBmpdPbiiiHlWjYIF499JfbKGUF-r8cXdB2a9j2Kldlwl_R1XrrB40txLvPymE57w&usqp=CAc"} 
                        rating={5} 
                    />
                </div>
                <div className="home_row">
                    <Product
                        title="Samsun Monitor LED - Il monitor da gaming che puoi avere tutto per te!
                        Cosa aspetti, l'offerta sta per scadere" 
                        price={299.99} 
                        image={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTmn4WwL8fZNMcoyNEXl4kW6sRqw0t21X3IFvsWGbWuBETMg7gq3fKYXznMbO1zfomcdnL6OYM&usqp=CAc"} 
                        rating={5} 
                    />
                </div>
            </div>
        </div>
    )
}

export default Home
